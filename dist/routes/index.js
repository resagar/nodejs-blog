"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var adminRouter = require("./admin");
var router = express.Router();
router.get('/', function (req, res) {
    res.render('index', { nombre: "Rene Garcia" });
});
router.use('/admin', adminRouter);
module.exports = router;
