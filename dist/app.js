"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var indexRouter = require("./routes");
var Sequelize = require('sequelize');
var sequelize = new Sequelize('blog', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb'
});
sequelize
    .authenticate()
    .then(function () {
    console.log('Connection has been established successfully.');
})
    .catch(function () {
    console.error('Unable to connect to the database:');
});
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/static', express.static('public'));
app.use('/files', express.static('files'));
app.set('view engine', 'pug');
app.use('/', indexRouter);
app.listen(3000, function () {
    console.log('el servidor esta funcionando');
});
