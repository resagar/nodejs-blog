import express = require('express')
import adminRouter = require('./admin')
const router:express.Router = express.Router()

router.get('/', (req, res) => {
    res.render('index',{ nombre: "Rene Garcia" })
})

router.use('/admin', <express.Router>adminRouter)


module.exports = router